package am.helix.task.interfaces

import android.view.View

interface OnArticleSelectedListener {
    fun onItemSelected(view : View, position: Int, metadataItem: Any)
}