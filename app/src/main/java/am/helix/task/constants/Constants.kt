package am.helix.task.constants

object Network {
    const val BASE_URL = "https://www.helix.am/"
    const val END_POINT = "temp/json.php"
}

object Database {
    const val DB_NAME = "articlesDB"
}

object Local {
    const val KEY_DATA = "model"
}