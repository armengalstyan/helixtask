package am.helix.task.article.view.fragment


import am.helix.task.NetworkManager.isNetworkAvailable
import am.helix.task.R
import am.helix.task.article.model.MetadataItem
import am.helix.task.article.view.activity.MainActivity
import am.helix.task.article.view.adapter.ArticleRecyclerViewAdapter
import am.helix.task.article.viewmodel.ArticleViewModel
import am.helix.task.interfaces.OnArticleSelectedListener
import am.helix.task.room.offlinearticles.entity.OfflineArticleEntity
import am.helix.task.room.offlinearticles.viewmodel.OfflineArticleViewModel
import am.helix.task.room.seenarticles.entity.SeenArticleModel
import am.helix.task.room.seenarticles.viewmodel.SeenArticleViewModel
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_articles.*

class ArticlesFragment : Fragment(R.layout.fragment_articles), OnArticleSelectedListener {

    private lateinit var articleViewModel: ArticleViewModel
    private lateinit var seenArticleViewModel: SeenArticleViewModel
    private lateinit var offlineArticlesViewModel: OfflineArticleViewModel

    private val articleAdapter by lazy {
        val articleAdapter = ArticleRecyclerViewAdapter(this)
        articles_recycler_view.layoutManager = LinearLayoutManager(context)
        articles_recycler_view.adapter = articleAdapter
        return@lazy articleAdapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViewModel()
        seenArticleViewModel.getSeenArticles()?.observe(this, Observer {
            articleAdapter.addSeenArticles(it)
            articleAdapter.notifyDataSetChanged()
        })
        if (isNetworkAvailable(activity?.baseContext)) {
            initRequest()
        } else {
            offlineArticlesViewModel.getOfflineArticles()?.observe(this, Observer {
                progress_bar.visibility = View.GONE
                articleAdapter.addArticles(it)
                articleAdapter.notifyDataSetChanged()
            })
        }
    }

    private fun initViewModel() {
        articleViewModel =
            ViewModelProviders.of(requireActivity()).get(ArticleViewModel::class.java)
        seenArticleViewModel =
            ViewModelProviders.of(requireActivity()).get(SeenArticleViewModel::class.java)
        offlineArticlesViewModel =
            ViewModelProviders.of(requireActivity()).get(OfflineArticleViewModel::class.java)
    }

    private fun initRequest() {
        articleViewModel.requestArticles()
        articleViewModel.getArticleLiveData()?.observe(this, Observer {
            if(it != null) {
                progress_bar.visibility = View.GONE
                if (offlineArticlesViewModel.getOfflineArticles()?.value == null) {
                    offlineArticlesViewModel.saveArticlesOffline(it.metadata)
                }
                articleAdapter.addArticles(it.metadata)
                articleAdapter.notifyDataSetChanged()
            }
        })
    }

    override fun onItemSelected(view: View, position: Int, metadataItem: Any) {
        articleViewModel.select(metadataItem)
        if (metadataItem is MetadataItem) {
            seenArticleViewModel.saveArticle(
                SeenArticleModel(metadataItem.date?.toLong()!!, metadataItem.title!!)
            )
        } else if (metadataItem is OfflineArticleEntity) {
            seenArticleViewModel.saveArticle(
                SeenArticleModel(metadataItem.date?.toLong()!!, metadataItem.title!!)
            )
        }

        (activity as MainActivity?)?.createFragment(
            R.id.fragment_container,
            DetailsFragment()
        )
    }

    override fun onDetach() {
        super.onDetach()
        articleAdapter.removeArticleSelectedListener()
    }
}
