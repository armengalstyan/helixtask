package am.helix.task.article.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MetadataItem(
    @field:SerializedName("date")
    val date: Int? = null,

    @field:SerializedName("coverPhotoUrl")
    val coverPhotoUrl: String? = null,

    @field:SerializedName("shareUrl")
    val shareUrl: String? = null,

    @field:SerializedName("category")
    val category: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("body")
    val body: String? = null,

    @field:SerializedName("gallery")
    val gallery: List<GalleryItem?>? = null,

    @field:SerializedName("video")
    val video: List<VideoItem?>? = null
) : Parcelable