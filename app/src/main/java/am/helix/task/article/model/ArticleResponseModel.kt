package am.helix.task.article.model

import com.google.gson.annotations.SerializedName

data class ArticleResponseModel(
	@field:SerializedName("metadata")
	val metadata: List<MetadataItem?>? = null,

	@field:SerializedName("internal_errors")
	val internalErrors: List<Any?>? = null,

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("errors")
	val errors: List<Any?>? = null
)