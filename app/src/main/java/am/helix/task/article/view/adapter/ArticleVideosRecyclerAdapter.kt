package am.helix.task.article.view.adapter

import am.helix.task.R
import am.helix.task.article.model.GalleryItem
import am.helix.task.article.model.VideoItem
import am.helix.task.article.view.viewholder.ArticleVideosViewHolder
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class ArticleVideosRecyclerAdapter : RecyclerView.Adapter<ArticleVideosViewHolder>() {

    private var mVideoItems : List<Any?>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleVideosViewHolder {
        return ArticleVideosViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.article_video_row_item, parent, false), parent.context)    }

    override fun getItemCount(): Int {
        return mVideoItems?.size ?: 0
    }

    override fun onBindViewHolder(holder: ArticleVideosViewHolder, position: Int) {
        holder.bind(mVideoItems?.get(position))
    }

    fun addVideoItems(videoItems : List<Any?>?) {
        mVideoItems = videoItems
    }
}