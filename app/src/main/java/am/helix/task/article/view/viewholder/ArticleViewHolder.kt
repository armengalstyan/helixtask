package am.helix.task.article.view.viewholder

import am.helix.task.article.model.MetadataItem
import am.helix.task.interfaces.OnArticleSelectedListener
import am.helix.task.room.offlinearticles.entity.OfflineArticleEntity
import am.helix.task.room.seenarticles.entity.SeenArticleModel
import android.graphics.BitmapFactory
import android.graphics.Color
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import kotlinx.android.synthetic.main.article_row_item.view.*
import java.sql.Timestamp
import java.util.*

class ArticleViewHolder(
    itemView: View,
    private val articleSelectedListener: OnArticleSelectedListener
) : RecyclerView.ViewHolder(itemView) {

    fun bind(metadataItem: Any?, seenArticleModels: List<SeenArticleModel?>?) {
        this.setIsRecyclable(false)
        if (metadataItem is MetadataItem) {
            itemView.setOnClickListener {
                articleSelectedListener.onItemSelected(it, adapterPosition, metadataItem)
            }

            if (seenArticleModels != null) {
                for (element in seenArticleModels) {
                    if (metadataItem.date?.toLong() == element?.timeStamp) {
                        itemView.card_view.setBackgroundColor(Color.LTGRAY)
                    }
                }
            }

            itemView.article_cover_img.load(
                "https" + metadataItem.coverPhotoUrl?.substring(
                    4,
                    metadataItem.coverPhotoUrl.length
                )
            )
            itemView.category_src.text = metadataItem.category
            itemView.title_src.text = metadataItem.title
            val timestamp = Timestamp(metadataItem.date?.toLong()!!)
            val date = Date(timestamp.time)
            itemView.date.text = date.toString()
        } else if (metadataItem is OfflineArticleEntity) {
            itemView.setOnClickListener {
                articleSelectedListener.onItemSelected(it, adapterPosition, metadataItem)
            }

            if (seenArticleModels != null) {
                for (element in seenArticleModels) {
                    if (metadataItem.date?.toLong() == element?.timeStamp) {
                        itemView.card_view.setBackgroundColor(Color.LTGRAY)
                    }
                }
            }

            val byteImage = metadataItem.coverPhotoUrl
            byteImage?.let {
                val decodeBitmap = BitmapFactory.decodeByteArray(byteImage, 0, byteImage.size)
                itemView.article_cover_img.load(decodeBitmap)
            }

            itemView.category_src.text = metadataItem.category
            itemView.title_src.text = metadataItem.title
            val timestamp = Timestamp(metadataItem.date?.toLong()!!)
            val date = Date(timestamp.time)
            itemView.date.text = date.toString()
        }
    }
}