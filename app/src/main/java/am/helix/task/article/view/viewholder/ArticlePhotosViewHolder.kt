package am.helix.task.article.view.viewholder

import am.helix.task.article.model.GalleryItem
import am.helix.task.room.offlinearticles.entity.OfflineGalleryItemEntity
import android.graphics.BitmapFactory
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import kotlinx.android.synthetic.main.article_photo_row_item.view.*


class ArticlePhotosViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(galleryItem: Any?) {
        if (galleryItem is GalleryItem) {
            itemView.photo_img.load(
                "https" + galleryItem.thumbnailUrl?.substring(
                    4,
                    galleryItem.thumbnailUrl.length
                )
            )
        } else if (galleryItem is OfflineGalleryItemEntity) {
            val byteImage = galleryItem.thumbnailUrl
            byteImage?.let {
                val decodeBitmap = BitmapFactory.decodeByteArray(byteImage, 0, byteImage.size)
                itemView.photo_img.load(decodeBitmap)
            }
        }
    }
}