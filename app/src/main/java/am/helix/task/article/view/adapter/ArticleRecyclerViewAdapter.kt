package am.helix.task.article.view.adapter

import am.helix.task.R
import am.helix.task.article.model.MetadataItem
import am.helix.task.article.view.viewholder.ArticleViewHolder
import am.helix.task.interfaces.OnArticleSelectedListener
import am.helix.task.room.seenarticles.entity.SeenArticleModel
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class ArticleRecyclerViewAdapter(private var articleSelectedListener : OnArticleSelectedListener?) : RecyclerView.Adapter<ArticleViewHolder>() {

    private var mArticles: List<Any?>? = null
    private var mSeenArticles: List<SeenArticleModel?>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        return ArticleViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.article_row_item,
                parent,
                false
            ), articleSelectedListener!!
        )
    }

    override fun getItemCount(): Int {
        return mArticles?.size ?: 0
    }

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        holder.bind(mArticles?.get(position), mSeenArticles)
    }

    fun addArticles(articles: List<Any?>?) {
        mArticles = null
        mArticles = articles
    }

    fun addSeenArticles(seenArticles: List<SeenArticleModel?>?) {
        mSeenArticles = null
        mSeenArticles = seenArticles
    }

    fun removeArticleSelectedListener() {
        articleSelectedListener = null
    }
}