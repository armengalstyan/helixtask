package am.helix.task.article.view.fragment


import am.helix.task.R
import am.helix.task.article.model.MetadataItem
import am.helix.task.article.view.activity.MainActivity
import am.helix.task.article.viewmodel.ArticleViewModel
import am.helix.task.constants.Local
import am.helix.task.constants.Local.KEY_DATA
import am.helix.task.room.offlinearticles.entity.OfflineArticleEntity
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import coil.api.load
import kotlinx.android.synthetic.main.fragment_details.*
import java.sql.Timestamp
import java.util.*

class DetailsFragment : Fragment(R.layout.fragment_details) {

    private lateinit var articleViewModel: ArticleViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViewModel()
        getSelectedArticle()
    }

    private fun initViewModel() {
        articleViewModel =
            ViewModelProviders.of(requireActivity()).get(ArticleViewModel::class.java)
    }

    private fun getSelectedArticle() {
        articleViewModel.getSelectedItem().observe(this, Observer {
            setDataToView(it)
        })
    }

    private fun setDataToView(metadataItem: Any) {
        if (metadataItem is MetadataItem) {
            image_detail.load("https" + metadataItem.coverPhotoUrl?.substring(4, metadataItem.coverPhotoUrl.length))

            val timestamp = Timestamp(metadataItem.date!!.toLong())
            val timestampToDate = Date(timestamp.time)
            date_detail.text = timestampToDate.toString()

            if (metadataItem.gallery?.size != null) {
                photo_library_img.visibility = View.VISIBLE
                photo_library_img.setOnClickListener {
                    val bundle: Bundle? = Bundle()
                    bundle?.putParcelable(KEY_DATA, metadataItem)
                    (activity as MainActivity?)?.createFragment(R.id.fragment_container, ArticlePhotosFragment(), bundle
                    )
                }
            }

            if (metadataItem.video?.size != null) {
                video_library_img.visibility = View.VISIBLE
                video_library_img.setOnClickListener {
                    val bundle: Bundle? = Bundle()
                    bundle?.putParcelable(KEY_DATA, metadataItem)
                    (activity as MainActivity?)?.createFragment(R.id.fragment_container, ArticleVideosFragment(), bundle)
                }
            }

            if (metadataItem.title != null) {
                title_detail_fragment.text = metadataItem.title
            }

            if (metadataItem.category != null) {
                category_detail_fragment.text = metadataItem.category
            }

            if (metadataItem.body != null) {
                body_detail_fragment.text = metadataItem.body

            }

            if (metadataItem.shareUrl != null) {
                shareUrl.text = metadataItem.shareUrl
            }
        } else if (metadataItem is OfflineArticleEntity) {
            val byteImage = metadataItem.coverPhotoUrl
            byteImage?.let {
                val decodeBitmap = BitmapFactory.decodeByteArray(byteImage, 0, byteImage.size)
                image_detail.load(decodeBitmap)
            }

            val timestamp = Timestamp(metadataItem.date!!.toLong())
            val timestampToDate = Date(timestamp.time)
            date_detail.text = timestampToDate.toString()

            if (metadataItem.gallery?.size != null) {
                photo_library_img.visibility = View.VISIBLE
                photo_library_img.setOnClickListener {
                    val bundle: Bundle? = Bundle()
                    bundle?.putParcelable("model", metadataItem)
                    (activity as MainActivity?)?.createFragment(R.id.fragment_container, ArticlePhotosFragment(), bundle)
                }
            }

            if (metadataItem.video?.size != null) {
                video_library_img.visibility = View.VISIBLE
                video_library_img.setOnClickListener {
                    val bundle: Bundle? = Bundle()
                    bundle?.putParcelable("model", metadataItem)
                    (activity as MainActivity?)?.createFragment(R.id.fragment_container, ArticleVideosFragment(), bundle)
                }
            }

            if (metadataItem.title != null) {
                title_detail_fragment.text = metadataItem.title
            }

            if (metadataItem.category != null) {
                category_detail_fragment.text = metadataItem.category
            }

            if (metadataItem.body != null) {
                body_detail_fragment.text = metadataItem.body
            }

            if (metadataItem.shareUrl != null) {
                shareUrl.text = metadataItem.shareUrl
                shareUrl.setOnClickListener {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(metadataItem.shareUrl)
                    startActivity(intent)
                }
            }
        }
    }
}

