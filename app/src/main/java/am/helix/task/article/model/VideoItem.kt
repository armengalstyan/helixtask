package am.helix.task.article.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VideoItem(
    @field:SerializedName("youtubeId")
    val youtubeId: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("thumbnailUrl")
    val thumbnailUrl: String? = null
) : Parcelable