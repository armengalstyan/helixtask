package am.helix.task.article.view.adapter

import am.helix.task.R
import am.helix.task.article.view.viewholder.ArticlePhotosViewHolder
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class ArticlePhotosRecyclerAdapter : RecyclerView.Adapter<ArticlePhotosViewHolder>() {

    private var mGalleryItems : List<Any?>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticlePhotosViewHolder {
        return ArticlePhotosViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.article_photo_row_item, parent, false))
    }

    override fun getItemCount(): Int {
        return mGalleryItems?.size ?:0
    }

    override fun onBindViewHolder(holder: ArticlePhotosViewHolder, position: Int) {
        holder.bind(mGalleryItems?.get(position))
    }

    fun addGalleryItems(galleryItems : List<Any?>?) {
        mGalleryItems = galleryItems
    }
}