package am.helix.task.article.viewmodel

import am.helix.task.article.model.ArticleResponseModel
import am.helix.task.article.model.MetadataItem
import am.helix.task.article.viewmodel.repository.ArticleRepository
import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class ArticleViewModel(application: Application) : AndroidViewModel(application) {

    private var articleRepository: ArticleRepository? = null
    private var articlesMutableLiveData : MutableLiveData<ArticleResponseModel> = MutableLiveData()
    private val selected = MutableLiveData<Any>()

    fun requestArticles() {
        if (articleRepository == null) {
            articleRepository = ArticleRepository()
        }
        articlesMutableLiveData = articleRepository?.initRequest()!!
    }

    fun getArticleLiveData(): LiveData<ArticleResponseModel>? {
        return articlesMutableLiveData
    }

    fun select(item: Any) {
        selected.value = item
    }

    fun getSelectedItem(): LiveData<Any> {
        return selected
    }
}