package am.helix.task.article.viewmodel.repository

import am.helix.task.article.model.ArticleResponseModel
import am.helix.task.webservice.ApiService
import am.helix.task.webservice.RetrofitClient
import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ArticleRepository {

    private val mutableLiveData : MutableLiveData<ArticleResponseModel> = MutableLiveData()

    fun initRequest(): MutableLiveData<ArticleResponseModel> {

        RetrofitClient.instance?.api?.getArticles()?.enqueue(object : Callback<ArticleResponseModel> {
            override fun onResponse(call: Call<ArticleResponseModel>, response: Response<ArticleResponseModel>) {
                if (response.isSuccessful && response.code() == 200 && response.body() != null) {
                    mutableLiveData.value = response.body()
                }
            }

            override fun onFailure(call: Call<ArticleResponseModel>, t: Throwable) {
                mutableLiveData.value = null
            }
        })
        return mutableLiveData
    }
}