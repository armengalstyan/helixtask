package am.helix.task.article.view.fragment


import am.helix.task.R
import am.helix.task.article.model.MetadataItem
import am.helix.task.article.view.adapter.ArticleVideosRecyclerAdapter
import am.helix.task.constants.Local.KEY_DATA
import am.helix.task.room.offlinearticles.entity.OfflineArticleEntity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import kotlinx.android.synthetic.main.fragment_article_videos.*


class ArticleVideosFragment : Fragment(R.layout.fragment_article_videos) {

    private val articleVideosAdapter by lazy {
        val articleVideosAdapter = ArticleVideosRecyclerAdapter()
        article_videos_recycler_view.layoutManager = LinearLayoutManager(activity?.baseContext)
        article_videos_recycler_view.adapter = articleVideosAdapter
        article_videos_recycler_view.setHasFixedSize(true)
        val snapHelper = PagerSnapHelper()
        snapHelper.attachToRecyclerView(article_videos_recycler_view)
        return@lazy articleVideosAdapter
    }

    private var metadataItem: Any? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        metadataItem  = arguments?.getParcelable(KEY_DATA)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(metadataItem is MetadataItem) {
            articleVideosAdapter.addVideoItems((metadataItem as MetadataItem).video!!)
        } else if(metadataItem is OfflineArticleEntity) {
            articleVideosAdapter.addVideoItems((metadataItem as OfflineArticleEntity).video!!)
        }
        articleVideosAdapter.notifyDataSetChanged()
    }
}
