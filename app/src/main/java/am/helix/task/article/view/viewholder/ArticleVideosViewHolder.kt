package am.helix.task.article.view.viewholder

import am.helix.task.NetworkManager
import am.helix.task.article.model.VideoItem
import am.helix.task.room.offlinearticles.entity.OfflineVideoItemEntity
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import kotlinx.android.synthetic.main.article_video_row_item.view.*

class ArticleVideosViewHolder(itemView: View, private val context: Context) :
    RecyclerView.ViewHolder(itemView) {
    fun bind(videoItem: Any?) {
        var youtubeUrl = "https://www.youtube.com/watch?v="

        if (videoItem is VideoItem) {
            itemView.video_img.load(
                "https" + videoItem.thumbnailUrl?.substring(
                    4,
                    videoItem.thumbnailUrl.length
                )
            )
            youtubeUrl += videoItem.youtubeId
        } else if (videoItem is OfflineVideoItemEntity) {
            val byteImage = videoItem.thumbnailUrl
            byteImage?.let {
                val decodeBitmap = BitmapFactory.decodeByteArray(byteImage, 0, byteImage.size)
                itemView.video_img.load(decodeBitmap)
            }
            youtubeUrl += videoItem.youtubeId!!
        }

        itemView.setOnClickListener {
            if (NetworkManager.isNetworkAvailable(context)) {
                val webIntent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(youtubeUrl)
                )
                itemView.context.startActivity(webIntent)
            } else {
                Toast.makeText(context, "Check your internet connection", Toast.LENGTH_LONG).show()
            }
        }
    }
}