package am.helix.task.article.view.fragment

import am.helix.task.R
import am.helix.task.article.model.MetadataItem
import am.helix.task.article.view.adapter.ArticlePhotosRecyclerAdapter
import am.helix.task.constants.Local.KEY_DATA
import am.helix.task.room.offlinearticles.entity.OfflineArticleEntity
import am.helix.task.room.offlinearticles.entity.OfflineGalleryItemEntity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_article_photos.*
import kotlinx.android.synthetic.main.fragment_articles.*

class ArticlePhotosFragment : Fragment(R.layout.fragment_article_photos) {

    private val articlePhotosAdapter by lazy {
        val articlePhotosAdapter = ArticlePhotosRecyclerAdapter()
        gallery_photos_recycler_view.layoutManager = LinearLayoutManager(activity?.baseContext, RecyclerView.HORIZONTAL, false)
        gallery_photos_recycler_view.adapter = articlePhotosAdapter
        gallery_photos_recycler_view.setHasFixedSize(true)
        val snapHelper = PagerSnapHelper()
        snapHelper.attachToRecyclerView(gallery_photos_recycler_view)
        return@lazy articlePhotosAdapter
    }

    private var metadataItem: Any? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        metadataItem = arguments?.getParcelable(KEY_DATA)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(metadataItem is MetadataItem) {
            articlePhotosAdapter.addGalleryItems((metadataItem as MetadataItem).gallery!!)
        } else if(metadataItem is OfflineArticleEntity) {
            articlePhotosAdapter.addGalleryItems((metadataItem as OfflineArticleEntity).gallery!!)
        }
        articlePhotosAdapter.notifyDataSetChanged()
    }
}
