package am.helix.task

import am.helix.task.article.model.MetadataItem
import am.helix.task.room.offlinearticles.entity.OfflineArticleEntity
import am.helix.task.room.offlinearticles.entity.OfflineGalleryItemEntity
import am.helix.task.room.offlinearticles.entity.OfflineVideoItemEntity
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import coil.Coil
import coil.api.get
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream

object Converter {
    suspend fun convertPojoToEntity(scope: CoroutineScope, metadataItem: List<MetadataItem?>): List<OfflineArticleEntity> {
        val a = scope.async {
            val offlineArticleEntityList = mutableListOf<OfflineArticleEntity>()

            for (i in metadataItem.indices) {
                val offlineArticleEntity = OfflineArticleEntity()

                offlineArticleEntity.apply {
                    coverPhotoUrl = convertByteArray(scope, metadataItem[i]?.coverPhotoUrl!!)
                    date = metadataItem[i]?.date
                    shareUrl = metadataItem[i]?.shareUrl
                    category = metadataItem[i]?.category
                    title = metadataItem[i]?.title
                    body = metadataItem[i]?.body

                    if (metadataItem[i]?.gallery != null) {
                        val galleryList = mutableListOf<OfflineGalleryItemEntity>()
                        for (j in 0 until metadataItem[i]?.gallery?.size!!) {
                            galleryList.add(OfflineGalleryItemEntity().apply {
                                scope.launch {
                                    thumbnailUrl = convertByteArray(scope, metadataItem[i]?.gallery?.get(j)?.thumbnailUrl!!)
                                }
                                contentUrl = metadataItem[i]?.gallery?.get(j)?.contentUrl
                                title = metadataItem[i]?.gallery?.get(j)?.title
                            })
                        }
                        gallery = galleryList.toList()
                    }

                    if (metadataItem[i]?.video != null) {
                        val videoList = mutableListOf<OfflineVideoItemEntity?>()
                        for (j in 0 until metadataItem[i]?.video?.size!!) {
                            videoList.add(OfflineVideoItemEntity().apply {

                                    thumbnailUrl = convertByteArray(scope, metadataItem[i]?.video?.get(j)?.thumbnailUrl!!)

                                title = metadataItem[i]?.video?.get(j)?.title
                                youtubeId = metadataItem[i]?.video?.get(j)?.youtubeId
                            })
                        }
                        video = videoList.toList()
                    }
                }

                offlineArticleEntityList.add(offlineArticleEntity)
            }
             offlineArticleEntityList
        }
        return a.await()
    }


    private suspend fun convertByteArray(scope : CoroutineScope, url: String) : ByteArray{
        val a = scope.async {
            val drawable = Coil.get(
                "https" + url.substring(4, url.length)
            ) {
                this.allowHardware(false)
            }
            val bitmap = (drawable as BitmapDrawable).bitmap
            getBytesFromBitmap(bitmap)
        }
        return a.await()
    }

    private fun getBytesFromBitmap(bitmap: Bitmap): ByteArray {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream)
        return stream.toByteArray()
    }
}
