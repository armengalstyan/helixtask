package am.helix.task.webservice

import am.helix.task.constants.Network
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitClient private constructor() {
    private val retrofit: Retrofit
    val api: ApiService
        get() = retrofit.create(ApiService::class.java)

    companion object {
        private const val BASE_URL = Network.BASE_URL
        private var mInstance: RetrofitClient? = null
        @get:Synchronized
        val instance: RetrofitClient?
            get() {
                if (mInstance == null) {
                    mInstance = RetrofitClient()
                }
                return mInstance
            }
    }

    init {
        retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}