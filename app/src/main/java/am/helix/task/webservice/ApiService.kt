package am.helix.task.webservice

import am.helix.task.article.model.ArticleResponseModel
import am.helix.task.constants.Network
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {
    @GET(Network.END_POINT)
    fun getArticles(): Call<ArticleResponseModel>
}