package am.helix.task.room.offlinearticles.entity

import am.helix.task.room.offlinearticles.converter.DataConverter
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
class OfflineArticleEntity : Parcelable{
    @ColumnInfo(name = "date")
    var date: Int? = null

    @ColumnInfo(name = "coverPhotoUrl", typeAffinity = ColumnInfo.BLOB)
    var coverPhotoUrl: ByteArray? = null

    @ColumnInfo(name = "shareUrl")
    var shareUrl: String? = null

    @ColumnInfo(name = "category")
    var category: String? = null

    @ColumnInfo(name = "title")
    var title: String? = null

    @ColumnInfo(name = "body")
    var body: String? = null

    @TypeConverters(DataConverter::class)
    @ColumnInfo(name = "gallery")
    var gallery: List<OfflineGalleryItemEntity?>? = null

    @TypeConverters(DataConverter::class)
    @ColumnInfo(name = "video")
    var video: List<OfflineVideoItemEntity?>? = null

    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    var uid: Int = 0
}