package am.helix.task.room.offlinearticles.viewmodel

import am.helix.task.Converter
import am.helix.task.article.model.MetadataItem
import am.helix.task.room.offlinearticles.entity.OfflineArticleEntity
import am.helix.task.room.offlinearticles.viewmodel.repository.OfflineArticleRepository
import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class OfflineArticleViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: OfflineArticleRepository? = OfflineArticleRepository(application)

    fun saveArticlesOffline(articlesModels: List<MetadataItem?>?) = viewModelScope.launch {
        val list = Converter.convertPojoToEntity(viewModelScope,  articlesModels!!)
        repository?.insertArticle(list)
    }

    fun getOfflineArticles(): LiveData<List<OfflineArticleEntity>>? {
        return repository?.getOfflineArticles()
    }
}
