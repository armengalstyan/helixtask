package am.helix.task.room.seenarticles.viewmodel

import am.helix.task.room.seenarticles.entity.SeenArticleModel
import am.helix.task.room.seenarticles.viewmodel.repository.SeenArticleRepository
import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class SeenArticleViewModel(application: Application) : AndroidViewModel(application) {

    private val repository : SeenArticleRepository? = SeenArticleRepository(application)

    fun saveArticle(seenArticleModel: SeenArticleModel) = viewModelScope.launch {
        repository?.insertArticle(seenArticleModel)
    }

    fun getSeenArticles() : LiveData<List<SeenArticleModel>>?{
        return repository?.getSeenArticles()
    }
}