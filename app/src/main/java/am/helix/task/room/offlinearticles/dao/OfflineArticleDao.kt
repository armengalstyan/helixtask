package am.helix.task.room.offlinearticles.dao

import am.helix.task.room.offlinearticles.entity.OfflineArticleEntity
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface OfflineArticleDao {
    @Query("SELECT * FROM OfflineArticleEntity")
    fun getOfflineArticles(): LiveData<List<OfflineArticleEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertArticles(offlineArticles: List<OfflineArticleEntity>)
}