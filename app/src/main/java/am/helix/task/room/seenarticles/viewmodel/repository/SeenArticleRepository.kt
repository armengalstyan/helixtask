package am.helix.task.room.seenarticles.viewmodel.repository

import am.helix.task.room.ArticleDatabase
import am.helix.task.room.seenarticles.entity.SeenArticleModel
import android.content.Context
import androidx.lifecycle.LiveData

class SeenArticleRepository(context : Context) {

    private val seenArticleDB = ArticleDatabase.getAppDataBase(context)
    private val seenArticleDao = seenArticleDB?.seenArticleDao()

    suspend fun insertArticle(articleModel: SeenArticleModel) {
        seenArticleDao?.insertArticle(articleModel)
    }

    fun getSeenArticles(): LiveData<List<SeenArticleModel>>? {
        return seenArticleDao?.getArticles()
    }
}