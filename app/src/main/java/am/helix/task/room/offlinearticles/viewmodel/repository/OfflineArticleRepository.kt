package am.helix.task.room.offlinearticles.viewmodel.repository

import am.helix.task.room.ArticleDatabase
import am.helix.task.room.offlinearticles.entity.OfflineArticleEntity
import android.content.Context
import androidx.lifecycle.LiveData

class OfflineArticleRepository(context: Context) {

    private val articleDB = ArticleDatabase.getAppDataBase(context)
    private val offlineArticleDao = articleDB?.offlineArticleDao()

    suspend fun insertArticle(articleEntities: List<OfflineArticleEntity>) {
        offlineArticleDao?.insertArticles(articleEntities)
    }

    fun getOfflineArticles(): LiveData<List<OfflineArticleEntity>>? {
        return offlineArticleDao?.getOfflineArticles()
    }
}