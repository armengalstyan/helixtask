package am.helix.task.room.offlinearticles.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
class OfflineVideoItemEntity(
    @ColumnInfo(name = "youtubeId")
    var youtubeId: String? = null,

    @ColumnInfo(name = "title")
    var title: String? = null,

    @ColumnInfo(name = "thumbnailUrl")
    var thumbnailUrl: ByteArray? = null
) : Parcelable