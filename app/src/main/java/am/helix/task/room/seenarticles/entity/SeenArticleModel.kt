package am.helix.task.room.seenarticles.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

//@Entity(indices = [Index(value = ["timeStamp"], unique = true)])
@Entity
class SeenArticleModel(@ColumnInfo(name = "timeStamp") var timeStamp: Long, @ColumnInfo(name = "title") var title: String) {

    @PrimaryKey(autoGenerate = true)
    var uid: Int = 0
}