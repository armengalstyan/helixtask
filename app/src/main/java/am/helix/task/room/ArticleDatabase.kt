package am.helix.task.room

import am.helix.task.room.offlinearticles.dao.OfflineArticleDao
import am.helix.task.room.offlinearticles.entity.OfflineArticleEntity
import am.helix.task.room.seenarticles.dao.SeenArticleDao
import am.helix.task.room.seenarticles.entity.SeenArticleModel
import android.content.Context
import androidx.room.RoomDatabase
import androidx.room.Database
import androidx.room.Room

@Database(entities = [SeenArticleModel::class, OfflineArticleEntity::class], version = 1)
abstract class ArticleDatabase : RoomDatabase() {
    abstract fun seenArticleDao(): SeenArticleDao
    abstract fun offlineArticleDao(): OfflineArticleDao

    companion object {
        private var INSTANCE: ArticleDatabase? = null

        fun getAppDataBase(context: Context): ArticleDatabase? {
            if (INSTANCE == null) {
                synchronized(ArticleDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        ArticleDatabase::class.java,
                        am.helix.task.constants.Database.DB_NAME
                    ).build()
                }
            }
            return INSTANCE
        }

        fun destroyDataBase() {
            INSTANCE = null
        }
    }
}