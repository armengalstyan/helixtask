package am.helix.task.room.offlinearticles.converter

import am.helix.task.room.offlinearticles.entity.OfflineGalleryItemEntity
import am.helix.task.room.offlinearticles.entity.OfflineVideoItemEntity
import androidx.room.TypeConverter
import com.google.gson.Gson

class DataConverter  {
    @TypeConverter
    fun galleryListToJson(value: List<OfflineGalleryItemEntity?>?): String {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun galleryJsonToList(value: String): List<OfflineGalleryItemEntity?>? {
        if(Gson().fromJson(value, Array<OfflineGalleryItemEntity?>::class.java) != null) {
            val objects =  Gson().fromJson(value, Array<OfflineGalleryItemEntity?>::class.java) as Array<OfflineGalleryItemEntity?>
            val list = objects.toList()
            return list
        }
        return null
    }

    @TypeConverter
    fun videoListToJson(value: List<OfflineVideoItemEntity?>?): String {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun videoJsonToList(value: String): List<OfflineVideoItemEntity?>? {
        if(Gson().fromJson(value, Array<OfflineVideoItemEntity?>::class.java) != null) {
            val objects = Gson().fromJson(
                value,
                Array<OfflineVideoItemEntity?>::class.java
            ) as Array<OfflineVideoItemEntity?>
            val list = objects.toList()
            return list
        }
       return null
    }
}