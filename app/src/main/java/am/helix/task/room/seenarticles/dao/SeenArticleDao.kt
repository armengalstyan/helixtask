package am.helix.task.room.seenarticles.dao

import am.helix.task.room.seenarticles.entity.SeenArticleModel
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface SeenArticleDao {
    @Query("SELECT * FROM SeenArticleModel")
    fun getArticles(): LiveData<List<SeenArticleModel>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertArticle(articleModel: SeenArticleModel)
}